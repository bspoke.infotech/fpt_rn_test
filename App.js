import * as React from "react";
import { Provider as PaperProvider } from "react-native-paper";
import { globalTheme } from "./src/styles/theme";
import AwesomeIcon from "react-native-vector-icons/FontAwesome5";
import NavigationController from "./src/navigation/NavigationController";

export default function App() {
	return (
		<PaperProvider
			theme={globalTheme}
			settings={{ icon: props => <AwesomeIcon {...props} /> }}
		>
			<NavigationController />
		</PaperProvider>
	);
}
