module.exports = {
	root: true,
	extends: ["prettier", "@react-native-community"],
	rules: {
		quotes: ["error", "double"],
		"comma-dangle": ["error", "never"],
		"prettier/prettier": "error"
	},
	plugins: ["prettier"]
};
