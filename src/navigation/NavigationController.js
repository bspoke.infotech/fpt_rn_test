import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LandingScreen } from "../screens/LandingScreen";
import HomeScreen from "../screens/HomeScreen";
import SplashScreen from "../screens/SplashScreen";
import { getAccessToken } from "../api/api";
import { sleep } from "../utils/utils";

const Stack = createNativeStackNavigator();
export default function NavigationController() {
	const [loading, setLoading] = useState(true);
	const [token, setToken] = useState("");

	useEffect(() => {
		const getToken = async () => {
			const val = await getAccessToken();
			if (val) {
				setToken(val);
			}
			setLoading(false);
		};
		sleep(1000).then(() => {
			getToken();
		});
	}, []);

	if (loading) {
		return <SplashScreen />;
	}

	return (
		<NavigationContainer>
			<Stack.Navigator
				screenOptions={{ headerShown: false }}
				initialRouteName={token ? "homePage" : "landingPage"}
			>
				<Stack.Screen name="landingPage" component={LandingScreen} />
				<Stack.Screen name="homePage" component={HomeScreen} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}
