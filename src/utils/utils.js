/**
 * Sleep time for execution to complete.
 * @param {number} ms - number of milliseconds to wait
 * @return {void}
 */
export function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
