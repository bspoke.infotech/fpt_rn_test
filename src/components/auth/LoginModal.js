/* eslint-disable no-alert */
import React, { useCallback, useState } from "react";
import { StyleSheet } from "react-native";
import { Button, Card, IconButton, TextInput } from "react-native-paper";
import { setAccessToken } from "../../api/api";
import { sleep } from "../../utils/utils";
import { ModalPopup } from "../modal/ModalPopup";
const styles = StyleSheet.create({
	containerStyle: {
		padding: 16,
		width: "80%",
		height: 320
	},
	topMargin: {
		marginTop: 16
	},
	cardActions: { justifyContent: "space-around", marginTop: 24 },
	cardContent: { flex: 2 }
});

const LoginModal = ({ isVisible, setVisible, navigation }) => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [maskPassword, setMaskPassword] = useState(true);
	const [loading, setLoading] = useState(false);
	const hideModal = () => setVisible(false);

	const handleLogin = useCallback(() => {
		if (!username) {
			alert("Name can't be empty");
			return;
		}
		if (!password) {
			alert("Password can't be empty");
			return;
		}
		setLoading(true);
		sleep(2000).then(async () => {
			if (username !== "Demo" || password !== "demo123") {
				alert("Invalid credentials");
				setLoading(false);
				return;
			}
			setLoading(false);
			await setAccessToken(username);
			navigation.replace("homePage");
		});
	}, [navigation, password, username]);

	return (
		<ModalPopup isVisible={isVisible}>
			<Card style={styles.containerStyle}>
				<Card.Title
					title="Enter your details"
					right={props => (
						<IconButton
							{...props}
							icon="times-circle"
							onPress={hideModal}
							disabled={loading}
						/>
					)}
				/>
				<Card.Content style={styles.cardContent}>
					<TextInput
						label="Username"
						placeholder="Enter username"
						value={username}
						onChangeText={text => setUsername(text)}
					/>
					<TextInput
						secureTextEntry={maskPassword}
						right={
							password && (
								<TextInput.Icon
									name="eye"
									onPress={() => setMaskPassword(val => !val)}
								/>
							)
						}
						style={styles.topMargin}
						placeholder="Enter password"
						label="Password"
						value={password}
						onChangeText={text => setPassword(text)}
					/>
				</Card.Content>
				<Card.Actions style={styles.cardActions}>
					<Button
						mode="contained"
						onPress={handleLogin}
						loading={loading}
						disabled={loading}
					>
						Login
					</Button>
				</Card.Actions>
			</Card>
		</ModalPopup>
	);
};

export default LoginModal;
