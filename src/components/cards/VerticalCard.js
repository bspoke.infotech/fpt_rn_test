import React from "react";
import { View, StyleSheet } from "react-native";
import {
	Card,
	IconButton,
	Paragraph,
	Surface,
	Title
} from "react-native-paper";

const styles = StyleSheet.create({
	surface: { width: 200, height: 320, marginHorizontal: 8 },
	cardContainer: { height: 320, width: 200 },
	cardCover: { flex: 5 },
	cardContent: {
		flex: 4,
		paddingLeft: 16,
		justifyContent: "space-between"
	},
	cardTitle: { flex: 1, fontSize: 16 },
	cardSubtitle: {
		flex: 1,
		marginLeft: 8
	},
	buttonContainer: {
		flex: 1,
		alignItems: "flex-end"
	},
	cardActions: {
		flex: 2,
		justifyContent: "space-between",
		alignItems: "center"
	}
});

export const VerticalCard = ({ title, points, handleAction, uri }) => {
	return (
		<Surface style={styles.surface}>
			<View style={styles.cardContainer}>
				<Card.Cover style={styles.cardCover} source={{ uri: uri }} />
				<View style={styles.cardContent}>
					<Title style={styles.cardTitle}>{title}</Title>
				</View>
				<Card.Actions style={styles.cardActions}>
					<View style={styles.cardSubtitle}>
						<Paragraph>Earn up to {points} pts</Paragraph>
					</View>
					<View style={styles.buttonContainer}>
						<IconButton
							color={"#bf103f"}
							icon="arrow-right"
							onPress={handleAction}
						/>
					</View>
				</Card.Actions>
			</View>
		</Surface>
	);
};
