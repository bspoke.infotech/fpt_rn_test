import React from "react";
import { View, StyleSheet } from "react-native";
import { Button, Card, Paragraph, Surface, Title } from "react-native-paper";

const styles = StyleSheet.create({
	surface: { width: 320, height: 160, marginHorizontal: 8 },
	cardContainer: { width: 320, height: 160, flexDirection: "row" },
	cardCover: { flex: 3 },
	cardContent: {
		flex: 4,
		paddingLeft: 16,
		justifyContent: "space-between"
	},
	cardTitle: { flex: 1, fontSize: 16 },
	cardSubtitle: { flex: 1, justifyContent: "flex-end", marginBottom: 16 },
	cardActions: { alignItems: "flex-end" }
});

export const HorizontalCard = ({ title, points, handleAction, uri }) => {
	return (
		<Surface style={styles.surface}>
			<View style={styles.cardContainer}>
				<Card.Cover style={styles.cardCover} source={{ uri: uri }} />
				<View style={styles.cardContent}>
					<Title style={styles.cardTitle}>{title}</Title>
					<View style={styles.cardSubtitle}>
						<Paragraph>Earn up to {points} pts</Paragraph>
					</View>
				</View>
				<Card.Actions style={styles.cardActions}>
					<Button icon="arrow-right" onPress={handleAction} />
				</Card.Actions>
			</View>
		</Surface>
	);
};
