import React from "react";
const { View } = require("react-native");
import Modal from "react-native-modal";
import globalStyles from "../../styles/styles";

export function ModalPopup({ children, isVisible }) {
	return (
		<View>
			<Modal isVisible={isVisible}>
				<View style={globalStyles.centerContainer}>{children}</View>
			</Modal>
		</View>
	);
}
