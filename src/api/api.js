import AsyncStorage from "@react-native-async-storage/async-storage";
import uuid from "react-native-uuid";
export const setAccessToken = async token => {
	await AsyncStorage.setItem("token", token);
};

export const getAccessToken = async () => {
	const token = await AsyncStorage.getItem("token");
	return token;
};

export const dataOfAssessments = [
	{
		title: "Advanced Health Screening",
		key: uuid.v4(),
		points: 1000,
		uri: "https://picsum.photos/230"
	},
	{
		title: "Basic Health Screening",
		key: uuid.v4(),
		points: 300,
		uri: "https://picsum.photos/201"
	},
	{
		title: "Moderate Health Screening",
		key: uuid.v4(),
		points: 600,
		uri: "https://picsum.photos/202"
	},
	{
		title: "Platinum Health Screening",
		key: uuid.v4(),
		points: 2000,
		uri: "https://picsum.photos/250"
	}
];

export const challengesData = [
	{
		title: "Say no to sugar",
		key: uuid.v4(),
		points: 50,
		uri: "https://picsum.photos/260"
	},
	{
		title: "5km challenge",
		key: uuid.v4(),
		points: 75,
		uri: "https://picsum.photos/275"
	},
	{
		title: "15km challenge",
		key: uuid.v4(),
		points: 60,
		uri: "https://picsum.photos/206"
	},
	{
		title: "Say no to fast food",
		key: uuid.v4(),
		points: 200,
		uri: "https://picsum.photos/285"
	}
];
