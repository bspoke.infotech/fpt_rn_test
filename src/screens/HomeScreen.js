import React, { useCallback, useEffect, useState } from "react";
import { FlatList, ScrollView, StyleSheet, View } from "react-native";
import { Appbar, Text } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { challengesData, dataOfAssessments, setAccessToken } from "../api/api";
import { HorizontalCard } from "../components/cards/HorizontalCard";
import { VerticalCard } from "../components/cards/VerticalCard";
import globalStyles from "../styles/styles";
import { sleep } from "../utils/utils";
import SplashScreen from "./SplashScreen";

const styles = StyleSheet.create({
	cardContainer: {
		padding: 20
	},
	sectionTitle: {
		fontSize: 24,
		marginLeft: 8,
		marginBottom: 16
	}
});

export const HomeScreen = ({ navigation }) => {
	const [assessmentData, setAssessmentData] = useState([]);
	const [activities, setActivities] = useState([]);
	const [loading, setLoading] = useState(true);
	const handleLogout = useCallback(async () => {
		await setAccessToken("");
		navigation.replace("landingPage");
	}, [navigation]);
	const renderAssessmentList = ({ item }) => {
		return (
			<HorizontalCard
				title={item.title}
				uri={item.uri}
				points={item.points}
				handleAction={() => console.log("handle")}
			/>
		);
	};

	const renderChallengesList = ({ item }) => {
		return (
			<VerticalCard
				title={item.title}
				uri={item.uri}
				points={item.points}
				handleAction={() => console.log("handle")}
			/>
		);
	};
	useEffect(() => {
		const fetchData = async () => {
			await sleep(1500);
			setAssessmentData([...dataOfAssessments]);
			setActivities([...challengesData]);
			setLoading(false);
		};
		fetchData();
	}, []);

	return (
		<SafeAreaView style={globalStyles.flexContainer}>
			<Appbar.Header>
				<Appbar.Content title="Title" />
				<Appbar.Action icon="sign-out-alt" onPress={handleLogout} />
			</Appbar.Header>
			{loading ? (
				<SplashScreen />
			) : (
				<ScrollView style={globalStyles.flexContainer}>
					<View style={styles.cardContainer}>
						<Text style={styles.sectionTitle}>Assesments</Text>
						<FlatList
							data={assessmentData}
							renderItem={renderAssessmentList}
							keyExtractor={item => item.key}
							horizontal={true}
						/>
					</View>
					<View style={styles.cardContainer}>
						<Text style={styles.sectionTitle}>Challenges</Text>
						<FlatList
							data={activities}
							renderItem={renderChallengesList}
							keyExtractor={item => item.key}
							horizontal={true}
						/>
					</View>
				</ScrollView>
			)}
		</SafeAreaView>
	);
};

export default HomeScreen;
