import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import globalStyles from "../styles/styles";
import AppLogo from "../assets/images/logo.png";
import { Avatar, Button } from "react-native-paper";
import LoginModal from "../components/auth/LoginModal";

const styles = StyleSheet.create({
	button: {
		marginTop: 75,
		width: 200
	},
	labelStyle: {
		fontSize: 24
	}
});
export const LandingScreen = ({ navigation }) => {
	const [showLoginModal, setShowLoginModal] = useState(false);
	const handleSignIn = () => setShowLoginModal(true);
	return (
		<View style={globalStyles.centerContainer}>
			<Avatar.Image size={150} source={AppLogo} />
			<Button
				icon="sign-in-alt"
				mode="contained"
				onPress={handleSignIn}
				style={styles.button}
				labelStyle={styles.labelStyle}
			>
				Login
			</Button>
			<LoginModal
				isVisible={showLoginModal}
				setVisible={setShowLoginModal}
				navigation={navigation}
			/>
		</View>
	);
};
