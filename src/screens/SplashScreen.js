import React from "react";
import { View } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import globalStyles from "../styles/styles";

export default function SplashScreen() {
	return (
		<View style={globalStyles.centerContainer}>
			<ActivityIndicator size="large" />
		</View>
	);
}
