import { DefaultTheme } from "react-native-paper";

export const globalTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: "#bf103f",
		accent: "yellow"
	}
};
