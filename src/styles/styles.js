import { StyleSheet } from "react-native";

const globalStyles = StyleSheet.create({
	centerContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	flexContainer: {
		flex: 1
	}
});

export default globalStyles;
