# Sample react native project for FPT

## Run the project

- Make sure you are using a **unix** based system.
- Install all the dependencies from react-native [website](https://reactnative.dev/docs/environment-setup#development-os)
- Clone repo with `git clone https://gitlab.com/bspoke.infotech/fpt_rn_test.git`
- Install project dependencies with `npm install`
- Attach your android/ios simulator
- For android, use `npx react-native run-android`
- For ios, first install pod with `npx pod-install` and then `npx react-native run-ios`
- To test login use the following credentials

```bash
username: Demo
password: demo123
```

Feel free to [contact me](tanvir1190@icloud.com) if facing any problem.
